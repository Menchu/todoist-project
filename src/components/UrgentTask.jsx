import React, { Component } from 'react'
import "../styles/urgentTask.scss";

export default class UrgentTask extends Component {
    render() {
        return (
          <div className="UrgentTask">
            <h2>Tareas Urgentes</h2>
            <div className="UrgentTask__list">
              <ul className="ulList">
                {this.props.urgent.map((urgentTask) => {
                  return (
                    <li key={urgentTask.id}>
                      <div className="ulList-left">
                        <p>{urgentTask.title}</p>
                      </div>
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
        );
      }
    }
    