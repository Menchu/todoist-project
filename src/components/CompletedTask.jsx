import React, { Component } from "react";
import "../styles/completedTask.scss";

export default class CompletedTask extends Component {
  render() {
    return (
      <div className="CompletedTask">
        <h2>Tareas Completadas</h2>
        <div className="CompletedTask__list">
          <ul className="ulList">
            {this.props.completed.map((taskCompleted) => {
              return (
                <li key={taskCompleted.id}>
                  <div className="ulList-left">
                    <p>{taskCompleted.title}</p>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}
