import React, { Component } from "react";
import "../styles/card.scss";

export default class Card extends Component {
  state = [
    {
      values: {
        id: "",
        title: "",
        description:"",
        type: "",
      },
    },
  ];

  handleSubmit = (ev) => {
    ev.preventDefault();

    const formValues = this.state.values;
    const objValues = Object.values(formValues);
    const validValues = objValues.filter((value) => Boolean(value));
    const isValid = objValues.length === validValues.length;

    if (isValid) {
      this.props.handleCreateTask(formValues);
      this.setState(this.state.values);
    } else {
      this.setState({
        error: "Todos los campos son requeridos",
      });
    }
  };
  handleOnChange = (ev) => {
    const name = ev.target.name;
    const type = ev.target.type;
    const value = ev.target.value;
    this.setState((prevState) => ({
      values: {
        ...prevState.values,
        [name]: value,
        [type]: value,
      },
    }));
  };

  render() {
    return (
      <form className="ToDoistForm" onSubmit={this.handleSubmit}>
        <div className="ToDoistForm__containerForm">
          <h3>Nueva Tarea</h3>
          <label htmlFor="title">
            <span>Nombre</span>
            <input
              className="inputName"
              type="text"
              name="title"
              value={this.state.values.title}
              required
              onChange={this.handleOnChange}
            />
          </label>
          <label htmlFor="description">
            <span>Descripción</span>
            <input
             
              type="text"
              name="description"
              value={this.state.values.description}
              required
              onChange={this.handleOnChange}
            />
          </label>

          <label htmlFor="type">
            <select
              id="type"
              onChange={this.handleOnChange}
              name="type"
              value={this.state.values.type}
            >
            <option value="">Seleciona una opción</option>
              <option value="Lifestyle">Lifestyle</option>
              <option value="Work">Work</option>
              <option value="Home">Home</option>
            </select>
          </label>
        </div>
        <div className="ToDoistForm__buttons">
          <button>Guardar tarea</button>
          <button onClick={this.props.toggleForm}>Cancelar</button>
        </div>
      </form>
    );
  }
}
