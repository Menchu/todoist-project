import React, { Component } from "react";
import {
  faPlus,
  faExclamationTriangle,
  faPencilAlt,
  faSave,
} from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Card from "./Card";
import "../styles/mainList.scss";

export default class MainList extends Component {
  state = {
    description:"",
    showForm: false,
    isEditing: false,
    editingId: null,
    showAddedTask: false,
    showCompletedTask: false,
  };

  toggleForm = () => {
    this.setState((prevState) => ({
      showForm: !prevState.showForm,
    }));
  };

  toggleShowTask = () => {
    this.setState((prevState) => ({
      showAddedTask: !prevState.showAddedTask,
    }));
  };

  handleOnChangeEditDescription = (ev) => {
    
    const name = ev.target.name;
    const value = ev.target.value;
    this.setState((prevState) => 
    ({ ...prevState, [name]: value }));
   
  };

  handleEditAndConfirm = (task) => {
    if (this.state.isEditing && this.state.editingId === task.id) {
      this.props.changeDescription(task.id, this.state.description);
      this.setState({
        isEditing: false,
        editingId: null,
        description: "",
      });
    } else {
      this.setState({
        isEditing: true,
        editingId: task.id,
        description: task.description,
      });
    }
  };

  render() {
    return (
      <section className="MainList">
        <h2>Bandeja de entrada</h2>
        <div className="MainList__list">
          {this.props.todos.map((task) => {
            return (
              <ul className="MainList__list--ulList" key={task.id}>
                <li className="container">
                <div className="container__left">
                <div>
                    <input
                      role="button"
                      type="radio"
                      onClick={() => this.props.SubmitRadioButton(task)}
                    ></input>
                    <p>{task.title}</p>
                    
                    </div>
                      {this.state.isEditing &&
                      this.state.editingId === task.id ? (
                        <textarea
                          name="description"
                          value={this.state.description}
                          onChange={this.handleOnChangeEditDescription}
                        />
                      ) : (
                        <p> {task.description}</p>
                      )}
                    
                      </div>

                  <div className="container__right">
                    <div className="container__right--Button">
                      <button
                        className="buttonConfirm"
                        type="button"
                        onClick={() => this.handleEditAndConfirm(task)}>
                        {this.state.isEditing && this.editingId === task.id ? (
                          <FontAwesomeIcon className="save" icon={faSave} />
                        ) : (
                          <FontAwesomeIcon className="" icon={faPencilAlt} />
                        )}
                      </button>
                      <FontAwesomeIcon
                      className="homeLinkUrgent"
                      icon={faExclamationTriangle}
                      role="button"
                      onClick={() => this.props.SubmitUrgentTask(task)}
                    />
                    </div>

                    <p className="liType">{task.type} </p>

                   
                  </div>
                </li>
              </ul>
            );
          })}
        </div>

        <div className="MainList__button">
          {this.state.showForm === false ? (
            <p className="MainList__button" onClick={this.toggleForm}>
              <FontAwesomeIcon className="plusIcon" icon={faPlus} />
              Añadir tarea
            </p>
          ) : (
            <Card
              handleCreateTask={this.props.handleCreateTask}
              toggleForm={this.toggleForm}
            />
          )}
        </div>
      </section>
    );
  }
}
