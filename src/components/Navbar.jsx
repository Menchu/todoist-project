import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationTriangle,faHome,faClipboardCheck } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import "../styles/navbar.scss";
export default class Navbar extends Component {
  render() {
    return (
      <div className="Navbar">
    <div className="Navbar__logo">
      <img src="/logo.png" alt="logo"></img>
   </div> 
   <div className="Navbar__containerLinks">
        <div className="Navbar__containerLinks--containerLeft">
          <div>
         
            <Link to="/">
              <FontAwesomeIcon className="homeLink" icon={faHome} />
            </Link>
          </div>

          <div className="searcher">
            <form className="searcherForm">
              <label htmlFor="search" className="searcherFormLabel" />
              <input 
              type="text" 
              name=""
              value={this.props.filter} 
              placeholder="búsqueda"
              onChange={this.props.HandleChangeFilter} 

              />
            </form>
          </div>
        </div>
        <div className="Navbar__containerLinks--containerRight">
          <div>
            <Link to="/completed">
            <FontAwesomeIcon className="urgentLink" icon={faClipboardCheck} />
            </Link>
          </div>
          <div>
            <Link to="/urgent">
              <FontAwesomeIcon className="urgentLink" icon={faExclamationTriangle} />
            </Link>
          </div>
        </div>
        </div>
      </div>
    );
  }
}
