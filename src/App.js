import React from "react";
import "./App.scss";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import MainList from "./components/MainList";
import CompletedTask from "./components/CompletedTask";
import UrgentTask from './components/UrgentTask';


import list from "./info.json";
// import {postCharacter} from './api/funcionality'

export default class App extends React.Component {
  state = {
    todos: list,
    completed: [],
    urgent: [],
    newDescriptionChanged:[],
    filter: "",
  };
  handleCreateTask = (newTask) => {
    this.setState((prevState) => ({
      todos: [...prevState.todos, newTask],
    }));
  };
  HandleChangeFilter =(ev)=>{
    const value = ev.target.value;
    this.setState({filter:value})

  }
 

  SubmitRadioButton = (taskCompleted) => {
    this.setState((prevState) => ({
      todos: prevState.todos.filter((todo) => todo.id !== taskCompleted.id),
      completed: [...prevState.completed, taskCompleted],
    }));
  };

 
  SubmitUrgentTask = (urgentTask) => { 
    this.setState((prevState) => ({
      todos: prevState.todos.filter((todo) => todo.id !== urgentTask.id),
      urgent: [...prevState.urgent, urgentTask],
    }));
  };

  changeDescription = (editId, newDescription,) => {
 
    this.setState((prevState) => ({
      todos: prevState.todos.map((task) => {
        if(task.id === editId ){
          return {...task, description:newDescription};
        } else {
          return task;
        }
      })
    }));
  };
 

  render() {
      const filteredCards = this.state.todos.filter((todo) =>{
      const title = todo.title.toLowerCase().trim();
      const filter = this.state.filter.toLowerCase().trim();

      return title.includes(filter);
    });
    return (
      <BrowserRouter>
        <section className="App">
          <Navbar HandleChangeFilter={this.HandleChangeFilter} filter={this.state.filter}/>
          <Switch>
            <Route
              path="/"
              exact
              component={(props) => (
                <MainList
                  {...props}
                  todos={filteredCards}
                  handleCreateTask={this.handleCreateTask}
                  completed={this.state.completed}
                  SubmitRadioButton={this.SubmitRadioButton}
                  HandleChangeFilter={this.HandleChangeFilter}
                  urgent={this.state.urgent}
                  SubmitUrgentTask={this.SubmitUrgentTask}
                  changeDescription={this.changeDescription}
                 

                />
              )}
            />
            <Route
              path="/completed"
              exact
              component={(props) => (
                <CompletedTask {...props} completed={this.state.completed} />
              )}
            />
            <Route
              path="/urgent"
              exact
              component={(props) => (
                <UrgentTask {...props} urgent={this.state.urgent} />
              )}
            />
          </Switch>
        </section>
      </BrowserRouter>
    );
  }
}
